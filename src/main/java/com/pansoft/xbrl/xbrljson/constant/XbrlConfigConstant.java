package com.pansoft.xbrl.xbrljson.constant;

/**
 * XBRL配置文件相关常量数据
 * @author coolmayi
 */
public class XbrlConfigConstant {

    /**
     * 元素值类型-字符串
     */
    public final static String elementValueType_String = "String";

    /**
     * 元素值类型-数值类型
     */
    public final static String elementValueType_Number = "number";

    /**
     * 元素值类型-日期类型
     */
    public final static String elementValueType_Date = "date";

    /**
     * 元素值类型-元组类型
     */
    public final static String elementValueType_Tuple = "tuple";

    /**
     * 上下文类型-时点类型
     */
    public final static String contextType_Instant = "instant";

    /**
     * 上下文类型-期间类型
     */
    public final static String contextType_Duration = "duration";

    /**
     * json配置节点根节点名称
     */
    public final static String jsonObjectNodeName = "jsonObject";

}

