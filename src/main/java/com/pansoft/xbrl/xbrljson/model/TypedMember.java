package com.pansoft.xbrl.xbrljson.model;

public class TypedMember extends ContextDim {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7040979903366277397L;

	
	public TypedMember() {
		this.setDimensionType("typedMember");
	}
	
//	/**
//	 * 轴元素信息 元素名称
//	 */
//	private String dimensionElemnet = null;
//	
//	/**
//	 * 轴元素信息 命名空间
//	 */
//	private String dimensionElementNsu = null;
//	
//	/**
//	 * 成员元素 元素名称
//	 */
//	private String memberElement = null;
//	/**
//	 * 成员元素 命名空间
//	 */
//	private String memberElementNsu = null;
	/**
	 * 成员值
	 */
	private String memberValue = null;

//	private List<DomNode> memberChildList = null;

	public String getMemberValue() {
		return memberValue;
	}

	public void setMemberValue(String memberValue) {
		this.memberValue = memberValue;
	}
	
//	public String getDimensionElemnet() {
//		return dimensionElemnet;
//	}
//
//	public void setDimensionElemnet(String dimensionElemnet) {
//		this.dimensionElemnet = dimensionElemnet;
//	}
//
//	public String getDimensionElementNsu() {
//		return dimensionElementNsu;
//	}
//
//	public void setDimensionElementNsu(String dimensionElementNsu) {
//		this.dimensionElementNsu = dimensionElementNsu;
//	}
//
//	public String getMemberElement() {
//		return memberElement;
//	}
//
//	public void setMemberElement(String memberElement) {
//		this.memberElement = memberElement;
//	}
//
//	public String getMemberElementNsu() {
//		return memberElementNsu;
//	}
//
//	public void setMemberElementNsu(String memberElementNsu) {
//		this.memberElementNsu = memberElementNsu;
//	}
//
//	public List<DomNode> getMemberChildList() {
//		return memberChildList;
//	}
//
//	public void setMemberChildList(List<DomNode> memberChildList) {
//		this.memberChildList = memberChildList;
//	}
//	
//	public void addMemberChildNode(DomNode item) {
//		if (item == null) {
//			return ;
//		}
//		if (memberChildList == null) {
//			memberChildList = new ArrayList<DomNode>();
//		}
//		memberChildList.add(item);
//	}
}
