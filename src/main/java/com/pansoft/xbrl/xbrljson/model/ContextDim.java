package com.pansoft.xbrl.xbrljson.model;


/**
 * 上下文维度信息
 * @author coolmayi
 *
 */
public class ContextDim extends BaseModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1131788610368208205L;

	private String dimensionType = "";
	
	/**
	 * 维度轴
	 */
	private String dimension = null;
	/**
	 * 维度成员
	 */
	private String member = null;

	public String getDimensionType() {
		return dimensionType;
	}

	public void setDimensionType(String dimensionType) {
		this.dimensionType = dimensionType;
	}

	public String getDimension() {
		return dimension;
	}

	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member;
	}

}
