package com.pansoft.xbrl.xbrljson.model;

import java.util.List;

/**
 * 维度分组
 * @author coolmayi
 * @create 2021/9/3
 */
public class ContextDimGruop extends BaseModel{

    /**
     * 维度编号
     */
    private String dimId = null;

    /**
     * 维度清单列表
     */
    private List<ContextDim> dimensionList;

    public String getDimId() {
        return dimId;
    }

    public void setDimId(String dimId) {
        this.dimId = dimId;
    }

    public List<ContextDim> getDimensionList() {
        return dimensionList;
    }

    public void setDimensionList(List<ContextDim> dimensionList) {
        this.dimensionList = dimensionList;
    }
}
