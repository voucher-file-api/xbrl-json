package com.pansoft.xbrl.xbrljson.model;


import com.pansoft.xbrl.xbrljson.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 单位
 * @author coolmayi
 *
 */
public class Unit extends BaseModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1155956116639043868L;
	
	/**
	 * 单值的单位类型，值为{@value}
	 */
	public final static int UnitTypeMeasure = 0;
	/**
	 * 比值的单位类型，值为{@value}
	 */
	public final static int UnitTypeDivide = 1;

	/**
	 * ID
	 */
	private String id = null;
	
	/**
	 * 明细项目
	 */
	private List<String> measureList = null;
	
	/**
	 * 分子定义列表
	 */
	private List<String> numeratorList = null;
	/**
	 * 分母定义列表
	 */
	private List<String> denominatorList = null;

	/**
	 * ex
	 */
	private int unitType = UnitTypeMeasure;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getMeasureList() {
		return measureList;
	}

	public void setMeasureList(List<String> measureList) {
		this.measureList = measureList;
	}
	

	public int getUnitType() {
		return unitType;
	}

	public void setUnitType(int unitType) {
		this.unitType = unitType;
	}

	public List<String> getNumeratorList() {
		return numeratorList;
	}

	public void setNumeratorList(List<String> numeratorList) {
		this.numeratorList = numeratorList;
	}

	public List<String> getDenominatorList() {
		return denominatorList;
	}

	public void setDenominatorList(List<String> denominatorList) {
		this.denominatorList = denominatorList;
	}

	public static int getUnittypemeasure() {
		return UnitTypeMeasure;
	}

	public static int getUnittypedivide() {
		return UnitTypeDivide;
	}
	
	/**
	 * 添加单位
	 * @param measure
	 */
	public void addMeasure(String measure) {
		
		if (StringUtil.isBlank(measure)) {
			return ;
		}
		
		if (measureList == null) {
			measureList = new ArrayList<String>();
		}
		
		measureList.add(measure);
	}
	
	/**
	 * 添加分子数据
	 * @param measure
	 */
	public void addNumerator(String measure) {
		
		if (StringUtil.isBlank(measure)) {
			return ;
		}
		
		if (numeratorList == null) {
			numeratorList = new ArrayList<String>();
		}
		
		numeratorList.add(measure);
	}
	
	/**
	 * 添加分母数据
	 * @param measure
	 */
	public void addDenominator(String measure) {
		
		if (StringUtil.isBlank(measure)) {
			return ;
		}
		
		if (denominatorList == null) {
			denominatorList = new ArrayList<String>();
		}
		
		denominatorList.add(measure);
	}
	
	
}
