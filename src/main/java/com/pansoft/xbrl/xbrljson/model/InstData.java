package com.pansoft.xbrl.xbrljson.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 实例文档数据
 * @author coolmayi
 *
 */
public class InstData extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1185537770824788850L;

	/**
	 * 数据类型
	 */
	public static final int VALUE_TYPE_NUMBER = 0;
	
	/**
	 * 数据类型
	 */
	public static final int VALUE_TYPE_STRING = 1;
	
	/**
	 * 数据类型
	 */
	public static final int VALUE_TYPE_TEXT = 2;

	/**
	 * 编号，可以用来绑定脚注信息
	 */
	private String id = null;
	
	/**
	 * 元素名称
	 */
	private String elementName = null;
	
	/**
	 * 元素命名空间
	 */
	private String elementNameSpaceUri = null;
	
	/**
	 * 元素命名空间前缀
	 */
	private String elementNsName = null;
	/**
	 * 上下文编号
	 */
	private String contextRef = null;
	/**
	 * 货币单位编号
	 */
	private String unitRef = null;
	/**
	 * 小数位数 小数点
	 */
	private String decimals = null;
	/**
	 * 有效位数，最右开始
	 */
	private String precision = null;
	/**
	 * 数据类型
	 */
	private int valueType;
	/**
	 * 数值数据
	 */
	private double numValue;
	/**
	 * 字符串数据
	 */
	private String charValue;
	/**
	 * 大文本数据
	 */
	private String strValue;
	/**
	 * 是否空值
	 */
	private String nil;
	/**
	 * 使用语言
	 */
	private String lang;
	/**
	 * 使用元组的根元素
	 */
	private String tupleID;
	/**
	 * 当前元组节点的父节点
	 */
	private String tupleParent;
	/**
	 * 元组数据分组序号
	 */
	private String tupleDataOrder;

	/**
	 * 下级数据列表
	 */
	private List<InstData> childList ;

	/**
	 * 元组完整路径
	 */
	private String tuplePath;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getElementName() {
		return elementName;
	}
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	public String getElementNameSpaceUri() {
		return elementNameSpaceUri;
	}
	public void setElementNameSpaceUri(String elementNameSpaceUri) {
		this.elementNameSpaceUri = elementNameSpaceUri;
	}
	public String getContextRef() {
		return contextRef;
	}
	public void setContextRef(String contextRef) {
		this.contextRef = contextRef;
	}
	public String getUnitRef() {
		return unitRef;
	}
	public void setUnitRef(String unitRef) {
		this.unitRef = unitRef;
	}
	public String getDecimals() {
		return decimals;
	}
	public void setDecimals(String decimals) {
		this.decimals = decimals;
	}
	public String getPrecision() {
		return precision;
	}
	public void setPrecision(String precision) {
		this.precision = precision;
	}
	public int getValueType() {
		return valueType;
	}
	public void setValueType(int valueType) {
		this.valueType = valueType;
	}
	public double getNumValue() {
		return numValue;
	}
	public void setNumValue(double numValue) {
		this.numValue = numValue;
	}
	public String getCharValue() {
		return charValue;
	}
	public void setCharValue(String charValue) {
		this.charValue = charValue;
	}
	public String getStrValue() {
		return strValue;
	}
	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}
	public String getNil() {
		return nil;
	}
	public void setNil(String nil) {
		this.nil = nil;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getTupleID() {
		return tupleID;
	}
	public void setTupleID(String tupleID) {
		this.tupleID = tupleID;
	}
	public String getTupleParent() {
		return tupleParent;
	}
	public void setTupleParent(String tupleParent) {
		this.tupleParent = tupleParent;
	}
	public String getTupleDataOrder() {
		return tupleDataOrder;
	}
	public void setTupleDataOrder(String tupleDataOrder) {
		this.tupleDataOrder = tupleDataOrder;
	}
	public String getTuplePath() {
		return tuplePath;
	}
	public void setTuplePath(String tuplePath) {
		this.tuplePath = tuplePath;
	}
	public String getElementNsName() {
		return elementNsName;
	}
	public void setElementNsName(String elementNsName) {
		this.elementNsName = elementNsName;
	}

	public List<InstData> getChildList() {
		return childList;
	}

	public void setChildList(List<InstData> childList) {
		this.childList = childList;
	}

	/**
	 * 添加下级节点
	 * @param instData
	 */
	public boolean addChild(InstData instData) {
		if (childList == null) {
			// 下级节点列表为空，不创建对象
			return false;
		}

		if (instData == null) {
			return false;
		}

		childList.add(instData);

		return true;
	}

	@Override
	public Object clone() {
		InstData instData = (InstData)super.clone();
		instData.setChildList(new ArrayList<InstData>());
		return instData;
	}
}
