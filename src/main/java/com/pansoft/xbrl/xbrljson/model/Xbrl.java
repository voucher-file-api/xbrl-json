package com.pansoft.xbrl.xbrljson.model;


import com.pansoft.xbrl.xbrljson.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * xbrl数据对象
 * @author coolmayi
 *
 */
public class Xbrl extends BaseModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3780456330152535271L;

	private String instant = null;
	
	private String startDate = null;
	
	private String endDate = null;
	
	
	/**
	 * 默认的实体信息
	 */
	private Entity defaultEntity = null;
	/**
	 * 环境对象
	 */
	private HashMap<String, String> paramMap = new HashMap<String, String>();
	
	/**
	 * 命名空间列表
	 */
	private HashMap<String, String> nameSpaceMap = new HashMap<String, String>();
	
	/**
	 * 引用入口文件
	 */
	private SchemaRef schemaRef = null;
	
	/**
	 * 文件头引用文件
	 */
	private HashSet<String> schemaLocationSet = new HashSet<String>();

	/**
	 * 上下文列表
	 */
	private HashMap<String, Context> contextMap = null;

	/**
	 * 货币单位列表
	 */
	private HashMap<String, Unit> unitList = null;


	/**
	 * 实例文档数据列表
	 */
	private List<InstData> instDataList = null;
	
	public HashMap<String, String> getNameSpaceMap() {
		return nameSpaceMap;
	}

	public void setNameSpaceMap(HashMap<String, String> nameSpaceMap) {
		this.nameSpaceMap = nameSpaceMap;
	}


	/**
	 * 添加命名空间数据
	 * @param nsName
	 * @param nsUri
	 */
	public void addNameSpace(String nsName, String nsUri) {
		
		if (StringUtil.isBlank(nsName) || StringUtil.isBlank(nsUri)) {
			return ;
		}
		if (nameSpaceMap == null) {
			nameSpaceMap = new HashMap<String, String>();
		}
		nameSpaceMap.put(nsName, nsUri);
	}
	
	
	public SchemaRef getSchemaRef() {
		return schemaRef;
	}

	public void setSchemaRef(SchemaRef schemaRef) {
		this.schemaRef = schemaRef;
	}

	public List<InstData> getInstDataList() {
		if (instDataList == null) {
			instDataList = new ArrayList<InstData>();
		}
		return instDataList;
	}

	public void setInstDataList(List<InstData> instDataList) {
		this.instDataList = instDataList;
	}

	public HashMap<String, String> getParamMap() {
		return paramMap;
	}

	public void setParamMap(HashMap<String, String> paramMap) {
		this.paramMap = paramMap;
	}

	/**
	 * 添加数据
	 * @param key
	 * @param value
	 */
	public void addParam(String key, String value) {
		if (paramMap == null) {
			paramMap = new HashMap<String, String>();
		}
		paramMap.put(key, value);
	}

	public String getInstant() {
		return instant;
	}

	public void setInstant(String instant) {
		this.instant = instant;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public HashMap<String, Unit> getUnitList() {
		if (unitList == null) {
			unitList = new HashMap<String, Unit>();
		}
		return unitList;
	}

	public void setUnitList(HashMap<String, Unit> unitList) {
		this.unitList = unitList;
	}

	public HashSet<String> getSchemaLocationSet() {
		if (schemaLocationSet == null) {
			schemaLocationSet = new HashSet<String>();
		}
		return schemaLocationSet;
	}

	public void setSchemaLocationSet(HashSet<String> schemaLocationSet) {
		this.schemaLocationSet = schemaLocationSet;
	}

	public Entity getDefaultEntity() {
		return defaultEntity;
	}

	public void setDefaultEntity(Entity defaultEntity) {
		this.defaultEntity = defaultEntity;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public HashMap<String, Context> getContextMap() {
		if (contextMap == null) {
			contextMap = new HashMap<String, Context>();
		}
		return contextMap;
	}

	public void setContextMap(HashMap<String, Context> contextMap) {
		this.contextMap = contextMap;
	}

	@Override
	public Object clone() {

		Xbrl xbrl = new Xbrl();
		if (defaultEntity != null) {
			xbrl.setDefaultEntity((Entity)defaultEntity.clone());
		}
		xbrl.setEndDate(this.getEndDate());
		xbrl.setInstant(this.getInstant());
		xbrl.setStartDate(this.getStartDate());
		if (paramMap != null) {
			xbrl.setParamMap((HashMap<String, String>)paramMap.clone());

		}
		if (nameSpaceMap != null) {
			xbrl.setNameSpaceMap((HashMap<String, String>)nameSpaceMap.clone());
		}

		if (schemaLocationSet != null) {
			xbrl.setSchemaLocationSet((HashSet<String>)schemaLocationSet.clone());
		}

		if (schemaRef != null) {
			xbrl.setSchemaRef((SchemaRef)schemaRef.clone());
		}

		if (unitList != null) {
			HashMap<String, Unit> unitMap = new HashMap<String, Unit>();
			for (Unit unit : unitList.values()) {
				unitMap.put(unit.getId(), unit);
			}
			xbrl.setUnitList(unitMap);
		}

//		xbrl.setContextMap();

		return xbrl;
	}
	
}
