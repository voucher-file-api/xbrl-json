package com.pansoft.xbrl.xbrljson.model;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * bean基础对象
 * @author coolmayi
 *
 */
public class BaseModel implements Serializable, Cloneable {
    private static Logger log = Logger.getLogger(BaseModel.class.toString());
    /**
     * 是否为表达式
     */
    public boolean isExp = false;

	/**
	 * 
	 */
	private static final long serialVersionUID = 2820083708414256740L;

    @Override
    public Object clone() {
        Object o = null;
        try {
            o = super.clone();
        } catch (CloneNotSupportedException e) {
            log.info(e.toString());
        }
        return o;
    }

    public boolean isExp() {
        return isExp;
    }

    public void setExp(boolean exp) {
        isExp = exp;
    }
}
