package com.pansoft.xbrl.xbrljson.model;


/**
 * 实体信息
 * 上下文里的entity属性
 * @author coolmayi
 *
 */
public class Entity extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5623971786278836546L;

	
	private String scheme = null;
	
	private String identifier = null;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	
}
