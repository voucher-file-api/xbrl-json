package com.pansoft.xbrl.xbrljson.model;

/**
 * 明确维度
 * @author coolmayi
 *
 */
public class ExplicitMember extends ContextDim { 

	/**
	 * 
	 */
	private static final long serialVersionUID = -6862350428756022786L;

	public ExplicitMember() {
		this.setDimensionType("explicitMember");
	}
	

	
//	/**
//	 * 轴元素元素名称
//	 */
//	private String dimensionElement = null;
//	/**
//	 * 轴元素 命名空间
//	 */
//	private String dimensionElementNsu = null;
//	
//	/**
//	 * 成员元素 元素名称
//	 */
//	private String memberElement = null;
//	/**
//	 * 成员元素 命名空间
//	 */
//	private String memberElementNsu = null;

//	public String getDimensionElement() {
//		return dimensionElement;
//	}
//
//	public void setDimensionElement(String dimensionElement) {
//		this.dimensionElement = dimensionElement;
//	}
//
//	public String getMemberElement() {
//		return memberElement;
//	}
//
//	public void setMemberElement(String memberElement) {
//		this.memberElement = memberElement;
//	}
//
//	public String getDimensionElementNsu() {
//		return dimensionElementNsu;
//	}
//
//	public void setDimensionElementNsu(String dimensionElementNsu) {
//		this.dimensionElementNsu = dimensionElementNsu;
//	}
//
//	public String getMemberElementNsu() {
//		return memberElementNsu;
//	}
//
//	public void setMemberElementNsu(String memberElementNsu) {
//		this.memberElementNsu = memberElementNsu;
//	}
}
