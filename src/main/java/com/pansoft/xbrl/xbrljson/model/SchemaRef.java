package com.pansoft.xbrl.xbrljson.model;


/**
 * schemaRef引用
 * @author coolmayi
 *
 */
public class SchemaRef extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3448017062515277337L;

	private String type = "simple";
	
	private String href = null;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}
	
}
