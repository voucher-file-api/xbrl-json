package com.pansoft.xbrl.xbrljson.model;


import java.util.ArrayList;
import java.util.List;

/**
 * 上下文信息
 * @author coolmayi
 *
 */
public class Context extends BaseModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2367844239777971350L;

	/**
	 * 背景期间类型为时点值，值为{@value}
	 */
	public final static int ContextPeriodInstant = 0;
	/**
	 * 背景期间类型为期间值，值为{@value}
	 */
	public final static int ContextPeriodDuration = 1;
	/**
	 * 背景期间为“永远”，值为{@value}
	 */
	public final static int ContextPeriodForever = 2;
	
	
	private String id = null;
	
	private String name = null;
	
	private String identifier = null;
	
	private String scheme = null;
	
	private String startDate = null;
	
	private String endDate = null;
	
	private String instDate = null;
	
	private List<ContextDim> scenarioList = null;
	
	private List<ContextDim> segmentList = null;
	
	/**
	 * 自定义的节点信息
	 */
	private ArrayList<String>	customScenario	=	new ArrayList<String>();
	private ArrayList<String>	customSegment	=	new ArrayList<String>();
	
	///////
	private int periodType = Context.ContextPeriodInstant;

	private String useDimension = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getInstDate() {
		return instDate;
	}

	public void setInstDate(String instDate) {
		this.instDate = instDate;
	}

	public List<ContextDim> getScenarioList() {
		return scenarioList;
	}

	public void setScenarioList(List<ContextDim> scenarioList) {
		this.scenarioList = scenarioList;
	}

	public List<ContextDim> getSegmentList() {
		return segmentList;
	}

	public void setSegmentList(List<ContextDim> segmentList) {
		this.segmentList = segmentList;
	}

	public ArrayList<String> getCustomScenario() {
		return customScenario;
	}

	public void setCustomScenario(ArrayList<String> customScenario) {
		this.customScenario = customScenario;
	}

	public ArrayList<String> getCustomSegment() {
		return customSegment;
	}

	public void setCustomSegment(ArrayList<String> customSegment) {
		this.customSegment = customSegment;
	}

	public int getPeriodType() {
		return periodType;
	}

	public void setPeriodType(int periodType) {
		this.periodType = periodType;
	}

	public String getUseDimension() {
		return useDimension;
	}

	public void setUseDimension(String useDimension) {
		this.useDimension = useDimension;
	}

	public static int getContextperiodinstant() {
		return ContextPeriodInstant;
	}

	public static int getContextperiodduration() {
		return ContextPeriodDuration;
	}

	public static int getContextperiodforever() {
		return ContextPeriodForever;
	}
	
}
