package com.pansoft.xbrl.xbrljson.config.model;

import com.pansoft.xbrl.xbrljson.model.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Json配置项
 * @author coolmayi
 * @create 2021/9/3
 */
public class JsonConfigItem extends BaseModel {

    /**
     * 字段名称
     */
    private String fieldName ;

    /**
     * 元素类型
     */
    private String elementValueType ;

    /**
     * 上下文类型
     */
    private String contextType;

    /**
     * 小数位数
     */
    private String decimals;

    /**
     * 货币类型
     */
    private String unitId;

    /**
     * 转换数据
     */
    private String controlData;

    /**
     * 元素名称
     */
    private String elementName;

    /**
     * 元素命名空间
     */
    private String elementNsName;

    /**
     * 下级节点列表
     */
    private List<JsonConfigItem> childList = null;

    public String getElementValueType() {
        return elementValueType;
    }

    public void setElementValueType(String elementValueType) {
        this.elementValueType = elementValueType;
    }

    public String getContextType() {
        return contextType;
    }

    public void setContextType(String contextType) {
        this.contextType = contextType;
    }

    public String getDecimals() {
        return decimals;
    }

    public void setDecimals(String decimals) {
        this.decimals = decimals;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getControlData() {
        return controlData;
    }

    public void setControlData(String controlData) {
        this.controlData = controlData;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementNsName() {
        return elementNsName;
    }

    public void setElementNsName(String elementNsName) {
        this.elementNsName = elementNsName;
    }

    public List<JsonConfigItem> getChildList() {
        return childList;
    }

    public void setChildList(List<JsonConfigItem> childList) {
        this.childList = childList;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * 添加下级节点
     * @param item
     */
    public void addChild(JsonConfigItem item) {

        if (item == null) {
            return;
        }

        if (childList == null) {
            childList = new ArrayList<JsonConfigItem>();
        }

        // 继承父节点属性
        if (item.getContextType() == null) {
            item.setContextType(this.getContextType());
        }
        if (item.getControlData() == null) {
            item.setControlData(this.getControlData());
        }
        if (item.getDecimals() == null) {
            item.setDecimals(this.getDecimals());
        }
        if (item.getElementName() == null) {
            item.setElementName(this.getElementName());
        }
        if (item.getElementNsName() == null) {
            item.setElementNsName(this.getElementNsName());
        }
        if (item.getElementValueType() == null) {
            item.setElementValueType(this.getElementValueType());
        }
        if (item.getUnitId() == null) {
            item.setUnitId(this.getUnitId());
        }


        childList.add(item);
    }
}
