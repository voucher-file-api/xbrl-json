package com.pansoft.xbrl.xbrljson.config;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import com.pansoft.xbrl.xbrljson.config.model.XbrlJsonConfig;


/**
 * 配置管理器
 * @author coolmayi
 */
public class XbrlJsonConfigMgr {

    private static volatile  XbrlJsonConfigMgr configMgr ;
	private static Logger log = Logger.getLogger(XbrlJsonConfigMgr.class.toString());

    /**
     * 配置项合集
     */
    private HashMap<String, XbrlJsonConfig> xbrlJsonConfigMap;

    public XbrlJsonConfigMgr() {
		initConfig();
    }

    /**
     * 获取配置管理器
     * @return
     */
    public static XbrlJsonConfigMgr getInstance() {
        if (configMgr == null ) {
            synchronized (XbrlJsonConfigMgr.class) {
                if (configMgr == null) {
                    configMgr = new XbrlJsonConfigMgr();
                }
            }
        }
        return configMgr;
    }

    /**
     * 根据配置编号获取配置对象
     * @param configId
     * @return
     */
    public XbrlJsonConfig getConfigObject(String configId) {

        if (xbrlJsonConfigMap == null) {
            this.initConfig();
        }

        return xbrlJsonConfigMap.get(configId);
    }

    /**
     * 初始化配置文件
     */
    private void initConfig() {

        if (xbrlJsonConfigMap == null) {
            xbrlJsonConfigMap = new HashMap<String, XbrlJsonConfig>();
        }
        File[] files = null;
        ArrayList<URL> urls = new ArrayList<URL>();
        Enumeration<URL> resources = null;
		try {
			resources = this.getClass().getClassLoader().getResources("xbrlJsonConfig");
		
	        while (resources.hasMoreElements()) {
	            URL url = resources.nextElement();
	            // 判断是否jar文件
	            if (url.getProtocol().equals("jar")) {
	                JarURLConnection urlConnection = (JarURLConnection) url.openConnection();
	                JarFile jarFile = urlConnection.getJarFile();
	                Enumeration<JarEntry> entries = jarFile.entries(); // 返回jar中所有的文件目录
	                while (entries.hasMoreElements()) {
	                    JarEntry jarEntry = entries.nextElement();
	                    if (!jarEntry.isDirectory() && jarEntry.getName().startsWith("xbrlJsonConfig")) {
	                    	String name = jarEntry.getName();
	                        Enumeration<URL> enums = this.getClass().getClassLoader().getResources(name);
							URL fileUrl = null;
							while(enums.hasMoreElements()){
								if((fileUrl = enums.nextElement()) == null ) continue;
								urls.add(fileUrl);
							}
	                    }
	                }
	            } else if (url.getProtocol().equals("file")) {
	                URL resource = this.getClass().getClassLoader().getResource("xbrlJsonConfig");
	                files = new File(resource.getPath()).listFiles();
	            }
	        }
		} catch (IOException e) {
			//e.printStackTrace();
			log.info(e.toString());
		}
		
		if(files != null) {
			for (File configFile : files) {
				XbrlJsonConfig configObject = this.createConfigObject(configFile);
				if (configObject == null) {
					continue;
				}
				xbrlJsonConfigMap.put(configObject.getConfigId(), configObject);
			}
		} else {
			for (URL url : urls) {
				XbrlJsonConfig configObject = this.createConfigObject(url);
				if (configObject == null) {
					continue;
				}
				xbrlJsonConfigMap.put(configObject.getConfigId(), configObject);
			}
		}
    }

    /**
     * 加载配置文件，生成配置文件对象
     * @param configFile
     * @return
     */
    private XbrlJsonConfig createConfigObject(File configFile) {

        if (configFile == null || !configFile.exists()) {
            return null;
        }
        XbrlJsonConfigReader configReader = new XbrlJsonConfigReader();
        return configReader.reafFile(configFile);
    }
    
    private XbrlJsonConfig createConfigObject(URL fileUrl) {
    	XbrlJsonConfigReader configReader = new XbrlJsonConfigReader();
    	return configReader.reafFile(fileUrl);
    }

}
