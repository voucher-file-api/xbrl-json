package com.pansoft.xbrl.xbrljson.config.model;

import com.pansoft.xbrl.xbrljson.constant.XbrlConfigConstant;
import com.pansoft.xbrl.xbrljson.model.BaseModel;
import com.pansoft.xbrl.xbrljson.model.ContextDimGruop;
import com.pansoft.xbrl.xbrljson.model.Xbrl;
import com.pansoft.xbrl.xbrljson.util.StringUtil;

import java.util.HashMap;
import java.util.List;

/**
 * 配置项管理器
 * @author coolmayi
 */
public class XbrlJsonConfig extends BaseModel {

    /**
     * 配置编号
     */
    private String configId;

    /**
     * 配置名称
     */
    private String configName;

    /**
     * xbrl数据模板，初始化数据
     */
    private Xbrl xbrlTemp;

    /**
     * json对象配置文件
     */
    private JsonConfigItem jsonConfig ;

    /**
     * 配置文件map
     */
    private HashMap<String, JsonConfigItem> jsonConfigMap;

    /**
     * 定义维度组列表
     */
    private HashMap<String, ContextDimGruop> contextDimGroupMap;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public Xbrl getXbrlTemp() {
        if (xbrlTemp == null) {
            xbrlTemp = new Xbrl();
        }
        return xbrlTemp;
    }

    public void setXbrlTemp(Xbrl xbrlTemp) {
        this.xbrlTemp = xbrlTemp;
    }

    public HashMap<String, ContextDimGruop> getContextDimGroupMap() {
        return contextDimGroupMap;
    }

    public void setContextDimGroupMap(HashMap<String, ContextDimGruop> contextDimGroupMap) {
        this.contextDimGroupMap = contextDimGroupMap;
    }

    public JsonConfigItem getJsonConfig() {
        return jsonConfig;
    }

    public void setJsonConfig(JsonConfigItem jsonConfig) {
        this.jsonConfig = jsonConfig;
        this.jsonConfigMap = null;
    }

    /**
     * 获取配置map对象
     * @return
     */
    public HashMap<String, JsonConfigItem>  getJsonConfigMap() {

        if (jsonConfig == null) {
            return null;
        }

        if (jsonConfigMap != null) {
            return jsonConfigMap;
        }
        jsonConfigMap = new HashMap<String, JsonConfigItem>();

        this.processJsonConfig(jsonConfigMap, jsonConfig, "");

        return jsonConfigMap;
    }

    /**
     * 加工处理配置对象
     * @param configItemMap
     * @param configItem
     * @param parentName
     */
    private void processJsonConfig(HashMap<String, JsonConfigItem> configItemMap, JsonConfigItem configItem, String parentName) {

        if (configItem == null) {
            return ;
        }
        String key = "";
        String fieldName = configItem.getFieldName();
        if (!XbrlConfigConstant.jsonObjectNodeName.equals(fieldName)) {
            if (StringUtil.isBlank(parentName)) {
                key = fieldName;
            } else {
                key = parentName + "-" + fieldName;
            }
        }

        configItemMap.put(key, configItem);

        List<JsonConfigItem> childList =  configItem.getChildList();

        if (childList == null) {
            return ;
        }

        for (JsonConfigItem item : childList) {
            this.processJsonConfig(configItemMap, item, key);
        }
    }





//    /**
//     * json对象配置文件
//     */
//    private JsonConfigItem jsonConfig ;
//
//    /**
//     * 配置文件map
//     */
//    private HashMap<String, JsonConfigItem> jsonConfigMap;
}
