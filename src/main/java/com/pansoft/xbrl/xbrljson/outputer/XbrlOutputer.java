package com.pansoft.xbrl.xbrljson.outputer;

import com.pansoft.xbrl.xbrljson.model.Xbrl;

/**
 * XBRL输出器
 * @author coolmayi
 *
 */
public interface XbrlOutputer {

	/**
	 * 生成xbrl数据
	 * @param savePath 保存文件目录
	 * @param xbrlData xbrl数据集合
	 * @param fileName 保存文件名
	 * @param exportTaxonomy 是否导出分类标准数据
	 * @return
	 * @throws Exception
	 */
	public boolean exportXbrl(Xbrl xbrlData, String savePath, String fileName, boolean exportTaxonomy) throws Exception;

	/**
	 * 生成xbrl数据-返回xml数据字符串
	 * @param xbrlData
	 * @return
	 * @throws Exception
	 */
	public String getXbrlXml(Xbrl xbrlData) throws Exception;

}
