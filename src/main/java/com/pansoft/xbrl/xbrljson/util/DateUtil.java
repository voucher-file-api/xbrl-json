package com.pansoft.xbrl.xbrljson.util;

import java.io.IOException;

/**
 * 日期类型工具栏
 * @author coolmayi
 *
 */
public class DateUtil {

	
//	public static void main(String[] args) {
//
//		System.out.println(formatDateValue("2020年1月20日"));
//		System.out.println(formatDateValue("2020年1月2日"));
//	}
	
	/**
	 * 
	 * @param dateValue
	 * @return
	 */
	public static String formatDateValue(String dateValue) {
		
		String ret = dateValue;
		
		if (!StringUtil.isBlank(ret)) {
			ret = ret.replaceAll("年", "-").replaceAll("月", "-").replaceAll("日", "");
			// 兼容几种处理
			String[] rets = ret.split("-");
			StringBuffer sb = new StringBuffer();
			for (String value : rets) {
				//try {
					if (value.length() < 2) {
						sb.append("0" + value);
					} else {
						sb.append(value);
					}
				//} catch (Exception e) {
					return ret;
				//}
			}
			ret = sb.toString();
			
			// 加工
			if (ret == null || ret.length() < 8 || ret.length() >= 10) {
				return ret;
			}
			
			if (ret.length() == 8) {
				ret = ret.substring(0,4)+"-"+ret.substring(4,6)+"-"+ret.substring(6,8);
			}
			
		}
		
		return ret;
	}
	
}
