package com.pansoft.xbrl.xbrljson.util;

import java.io.*;
import java.util.logging.Logger;

/**
 * @author coolmayi
 * @create 2021/9/6
 */
public class FileUtil {
    private static Logger log = Logger.getLogger(FileUtil.class.toString());
    /**
     * 获取json文件内容
     * @param filePath
     * @return
     */
    public static String readFile(String filePath) throws IOException {
        StringBuffer sb = new StringBuffer();
        InputStreamReader read = null;
        BufferedReader bufferedReader = null;
        try {
            String encoding = "UTF-8";
            File file = new File(filePath);
            if (file.isFile() && file.exists()) {
                read = new InputStreamReader(new FileInputStream(file), encoding);
                bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while ((lineTxt = bufferedReader.readLine()) != null) {
                    sb.append(lineTxt);
                }
            } else {
                throw new IOException("找不到指定的文件");
            }
        } catch (IOException e) {
            log.info(e.toString());
//            e.printStackTrace();
        }finally {
            if(null!=bufferedReader){
                bufferedReader.close();
            }
            if (null!=read) {
                read.close();
            }
        }
        return sb.toString();
    }
}
