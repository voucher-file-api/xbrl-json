package com.pansoft.xbrl.xbrljson.util;

/**
 * 
 * @author coolmayi
 *
 */
public class StringUtil {

	/**
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str) {
		int strLen = 0;
		if (str != null) {
			strLen = str.length();
			if ((strLen) == 0) {
				return true;
			}
		}

		for (int i = 0; i < strLen; ++i) {
			if (!(Character.isWhitespace(str.charAt(i)))) {
				return false;
			}
		}
		return true;
	}
}
