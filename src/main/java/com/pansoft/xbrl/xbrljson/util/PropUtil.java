package com.pansoft.xbrl.xbrljson.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * resources下属性管理器
 * @author coolmayi
 *
 */
public class PropUtil {

	private static Logger log = Logger.getLogger(PropUtil.class.toString());
	/**
	 * 属性数据map
	 */
	public static HashMap<String, Properties> propFileMap = null;
	
	
	
	/**
	 * 根据文件名获取属性对象
	 * @param propFileName
	 * @return
	 */
	public static Properties getPropObject(String propFileName) {
		if (propFileMap == null || !propFileMap.containsKey(propFileName)) {
			initFileMap(propFileName);
		}
		return propFileMap.get(propFileName);
	}
	
	
	/**
	 * 获取属性信息
	 * @param propFilename
	 * @param key
	 * @return
	 */
	public static String getPropValue(String propFilename, String key) {
		return getPropValue(propFilename, key, "");
	}
	
	
	/**
	 * 获取属性信息
	 * @param propFilename
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static String getPropValue(String propFilename, String key, String defValue) {
		
		Properties propObject = getPropObject(propFilename);
		if (propObject == null) {
			return defValue;
		}
		
		String retValue = propObject.getProperty(key);
		
		if (!StringUtil.isBlank(retValue)) {
			return retValue;
		}
		
		return defValue;
	}
	
	
	/**
	 * 加载属性文件
	 * @param fileName
	 */
	public synchronized static void initFileMap(String fileName) {
		
		if (StringUtil.isBlank(fileName)) {
			return ;
		}
		
		Properties properties = new Properties();
		InputStream in = null;
		try {
			in = PropUtil.class.getClassLoader().getResourceAsStream(fileName + ".properties");
			properties.load(in);
			
			if (propFileMap == null) {
				propFileMap = new HashMap<String, Properties>();
			}
			propFileMap.put(fileName, properties);
		} catch (IOException e) {
			//e.printStackTrace();
			log.info(e.toString());
		}finally {
			if(null!=in){
				safeCloseInputStream(in);
			}
		}
	}

	public static void safeCloseInputStream(InputStream fis) {
		if (fis != null) {
			try {
				fis.close();
			} catch (IOException e) {
				//e.printStackTrace();
				log.info(e.toString());
			}
		}
	}
}
