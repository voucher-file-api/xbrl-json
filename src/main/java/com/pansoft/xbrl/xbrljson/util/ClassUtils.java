package com.pansoft.xbrl.xbrljson.util;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

//import org.apache.log4j.Logger;

/**
 * 类工具
 *
 * @author ZhaoKai
 */
@SuppressWarnings("rawtypes")
public class ClassUtils {

//	private static final Logger log = Logger.getLogger(ClassUtils.class);
    private static Logger log = Logger.getLogger(ClassUtils.class.toString());
	private static final String UTF8 = "UTF-8";
	
//    private static final String BASE_PACKAGE_NAME = "com.pansoft.xbrl";

    private static final String JAR = "jar";

    private static final String FILE = "file";

    private static final String ZIP = "zip";

    private static final String VFS = "vfs";

    private static final String POINT = ".";

    private static Set<Class<?>> allClassSet = null;

    
	public static Set<Class<?>> getByInterface(Class clazz) {
    	String packageName = clazz.getPackage().getName();
        return getByInterface(clazz, packageName);
    }

    @SuppressWarnings("unchecked")
	public static Set<Class<?>> getByInterface(Class clazz, String packageName) {
        Set<Class<?>> classes = new LinkedHashSet<Class<?>>();
        // 获取指定接口的实现类
        if (clazz.isInterface()) {
            try {
                Set<Class<?>> classesAll = getClasses(packageName);
                /**
                 	* 循环判断路径下的所有类是否继承了指定类 并且排除父类自己
                 */
                Iterator<Class<?>> iterator = classesAll.iterator();
                while (iterator.hasNext()) {
                    Class<?> cls = iterator.next();
                    if(cls.getPackage().getName().startsWith(packageName)) {
                        /**
                         * isAssignableFrom该方法的解析，请参考博客：
                         * http://blog.csdn.net/u010156024/article/details/44875195
                         */
                        if (clazz.isAssignableFrom(cls)) {
                            if (!clazz.equals(cls)) {
                                classes.add(cls);
                            }
                        }
                    }

                }
            } catch (Exception e) {
                //System.out.println("出现异常");
                log.info("出现异常"+e);
            }
        }
        return classes;
    }

    /**
     * 从包package中获取所有的Class
     *
     * @return
     */
    public static Set<Class<?>> getClasses(String packageName) {
        if (null != allClassSet) {
            return allClassSet;
        }
        // 第一个class类的集合
        Set<Class<?>> classes = new LinkedHashSet<Class<?>>();
        // 获取包的名字 并进行替换
//        String packageName = BASE_PACKAGE_NAME;
        String packageDirName = packageName.replace('.', '/');

        // 定义一个枚举的集合 并进行循环来处理这个目录下的things
        Enumeration<URL> dirs = null;
        try {
            dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
        } catch (IOException e) {
            log.info(e.toString());
        }

        if(null==dirs){
            return classes;
        }
        // 循环迭代下去
        while (dirs.hasMoreElements()) {
            // 获取下一个元素
            URL url = dirs.nextElement();
            // 得到协议的名称
            String protocol = url.getProtocol();
            // 如果是以文件的形式保存在服务器上
            if (FILE.equals(protocol)) {
                // 获取包的物理路径
                String filePath = null;
                try {
                    filePath = URLDecoder.decode(url.getFile(), UTF8);
                } catch (UnsupportedEncodingException e) {
                    log.info(e.toString());
                }
                // 以文件的方式扫描整个包下的文件 并添加到集合中
                findAndAddClassesInPackageByFile(packageName, filePath, classes);
            } else if (JAR.equals(protocol)) {
                // 如果是jar包文件
                // 定义一个JarFile
                JarFile jar;
                try {
                    // 获取jar
                    jar = ((JarURLConnection) url.openConnection()).getJarFile();
                    searchClass(jar, packageDirName, classes);
                } catch (IOException e) {
                    //e.printStackTrace();
                    log.info(e.toString());
                }
            } else if (ZIP.equals(protocol)) {
                // 兼容 weblogic 将jar包定义为zip的问题
                String file = url.getFile();
                file = file.substring(0, file.lastIndexOf(POINT + JAR) + 4);
                JarFile jar = null;
                try {
                    jar = new JarFile(file);
                    searchClass(jar, packageDirName, classes);
                } catch (IOException e) {
                    //e.printStackTrace();
                    log.info(e.toString());
                } finally {
                    if (jar != null) {
                        safeClose(jar);
                    }
                }
            } else if(VFS.equals(protocol)) {
                // 兼容 wildfly 将包和jar包定义为vfs的问题
                String file = url.getFile();
                if(file.indexOf(POINT + JAR) > -1) {
                    // jar包
                    file = file.substring(0, file.lastIndexOf(POINT + JAR) + 4);
                    JarFile jar = null;
                    try {
                         jar = new JarFile(file);
                        searchClass(jar, packageDirName, classes);
                    } catch (IOException e) {
                        //e.printStackTrace();
                        log.info(e.toString());
                    }finally {
                        if (jar != null) {
                            safeClose(jar);
                        }
                    }
                } else {
                    // 包
                    // 获取包的物理路径
                    String filePath = null;
                    try {
                        filePath = URLDecoder.decode(url.getFile(), UTF8);
                    } catch (UnsupportedEncodingException e) {
                        log.info(e.toString());
                    }
                    // 以文件的方式扫描整个包下的文件 并添加到集合中
                    findAndAddClassesInPackageByFile(packageName, filePath, classes);
                }

            }
        }
        allClassSet = classes;
        return classes;
    }

    private static void searchClass(JarFile jar, String packageDirName, Set<Class<?>> classes) {
        Enumeration<JarEntry> entries = jar.entries();
        // 同样的进行循环迭代
        while (entries.hasMoreElements()) {
            // 获取jar里的一个实体 可以是目录 和一些jar包里的其他文件 如META-INF等文件
            JarEntry entry = entries.nextElement();
            String name = entry.getName();
            // 如果是以/开头的
            if (name.charAt(0) == '/') {
                // 获取后面的字符串
                name = name.substring(1);
            }
            // 如果前半部分和定义的包名相同
            if (name.startsWith(packageDirName)) {
                int idx = name.lastIndexOf('/');
                // 如果以"/"结尾 是一个包
                String jarPackageName = null;
                if (idx != -1) {
                    // 获取包名 把"/"替换成"."
                    jarPackageName = name.substring(0, idx).replace('/', '.');
                }
                if(null==jarPackageName){
                    continue;
                }
                // 如果可以迭代下去 并且是一个包
                if ((idx != -1)) {
                    // 如果是一个.class文件 而且不是目录
                    if (name.endsWith(".class") && !entry.isDirectory()) {
                        // 去掉后面的".class" 获取真正的类名
                        String className = name.substring(jarPackageName.length() + 1, name.length() - 6);
                        try {
                            // 添加到classes
                            classes.add(Class.forName(jarPackageName + '.' + className));
                        } catch (Error er) {
                        	//er.printStackTrace();
                            log.info(er.toString());
//                            log.error(er.toString());
//                            log.error(jarPackageName + '.' + className);
                        } catch (Exception e) {
                        	//e.printStackTrace();
                            log.info(e.toString());
//                            log.error(e.toString());
//                            log.error(jarPackageName + '.' + className);
                        }
                    }
                }
            }
        }
    }

    /**
     * 以文件的形式来获取包下的所有Class
     *
     * @param packageName
     * @param packagePath
     * @param classes
     */
    public static void findAndAddClassesInPackageByFile(String packageName, String packagePath, Set<Class<?>> classes) {
        // 获取此包的目录 建立一个File
        File dir = new File(packagePath);
        // 如果不存在或者 也不是目录就直接返回
        if (!dir.exists() || !dir.isDirectory()) {
            // log.warn("用户定义包名 " + packageName + " 下没有任何文件");
            return;
        }
        // 如果存在 就获取包下的所有文件 包括目录
        File[] dirFiles = dir.listFiles(new FileFilter() {
            // 自定义过滤规则 如果可以循环(包含子目录) 或则是以.class结尾的文件(编译好的java类文件)
            @Override
            public boolean accept(File file) {
                return (file.isDirectory()) || (file.getName().endsWith(".class"));
            }
        });
        // 循环所有文件
        for (File file : dirFiles) {
            // 如果是目录 则继续扫描
            if (file.isDirectory()) {
                findAndAddClassesInPackageByFile(packageName + "." + file.getName(), file.getAbsolutePath(), classes);
            } else {
                // 如果是java类文件 去掉后面的.class 只留下类名
                String className = file.getName().substring(0, file.getName().length() - 6);
                try {
                    // 添加到集合中去
                    // classes.add(Class.forName(packageName + '.' + className));
                    // 经过回复同学的提醒，这里用forName有一些不好，会触发static方法，没有使用classLoader的load干净
                    classes.add(
                            Thread.currentThread().getContextClassLoader().loadClass(packageName + '.' + className));
                } catch (ClassNotFoundException e) {
                    //e.printStackTrace();
                    log.info(e.toString());
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------------
    
    public static void safeClose(JarFile jf) {
        if (jf != null) {
            try {
                jf.close();
            } catch (IOException e) {
                //e.printStackTrace();
                log.info(e.toString());
            }
        }
    }


}
