package com.pansoft.xbrl.xbrljson.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author coolmayi
 * @create 2021/9/5
 */
public class PatternUtil {


    public static final String patternValue = "\\$\\{[^}]+\\}";
    public static final  Pattern pattern = Pattern.compile(patternValue);

    /**
     * 匹配参数 ${xxx}
     * @param exp
     * @return
     */
    public static List<String> getFiledExpList(String exp) {

        // 参数为空
        if (StringUtil.isBlank(exp)) {
            return null;
        }
        // 没有表达式
        if (exp.indexOf("${") < 0) {
            return null;
        }

        List<String> retList = new ArrayList<String>();
        Matcher matcher = pattern.matcher(exp);
        while(matcher.find()) {
            retList.add(matcher.group(0));
        }
        return retList;
    }

}
