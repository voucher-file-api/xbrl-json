package com.pansoft.xbrl.xbrljson.convert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.pansoft.xbrl.xbrljson.config.XbrlJsonConfigMgr;
import com.pansoft.xbrl.xbrljson.config.model.XbrlJsonConfig;
import com.pansoft.xbrl.xbrljson.model.Xbrl;
import com.pansoft.xbrl.xbrljson.outputer.XbrlOutputer;
import com.pansoft.xbrl.xbrljson.outputer.impl.BaseXbrlOutputer;
import com.pansoft.xbrl.xbrljson.util.StringUtil;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * json数据转换为xbrl数据
 * @author coolmayi
 * @create 2021/9/5
 */
public class JsonToXbrl {
    private static Logger log = Logger.getLogger(JsonToXbrl.class.toString());
    /**
     * xbrl输出
     */
    private XbrlOutputer xbrlOutputer = new BaseXbrlOutputer();


    /**
     * 创就按XBRL对象
     * @param jsonValue
     * @param configId
     * @return
     */
    public Xbrl createXbrlData(String jsonValue, String configId) {
        if (StringUtil.isBlank(jsonValue)) {
            return null;
        }
        JSONObject jsonObject;
        try {
            jsonObject = JSONObject.parseObject(jsonValue, Feature.OrderedField);
            return createXbrlData(jsonObject, configId);
        }  catch (Exception e) {
            e.printStackTrace();;
        }
        return null;
    }

    /**
     * 创建XBRL对象
     * @param jsonObject
     * @param configId
     * @return
     */
    public Xbrl createXbrlData(JSONObject jsonObject, String configId ) {

        if (jsonObject == null) {
            return null;
        }

        if (StringUtil.isBlank(configId)) {
            return null;
        }
        XbrlJsonConfig configObject = XbrlJsonConfigMgr.getInstance().getConfigObject(configId);
        if (configObject == null) {
            //System.out.println("没有找到配置文件信息，配置编号["+configId+"]");
            log.info("没有找到配置文件信息，配置编号["+configId+"]");
            return null;
        }

        Xbrl retXbrl = null;

        retXbrl = XbrlObjectCreator.getInstance().createXbrlData(configObject, jsonObject);

        return retXbrl;
    }

    /**
     * 转换实例文档xml数据
     * @param jsonValue
     * @param configId
     * @return
     * @throws Exception
     */
    public String convertXbrlXml(String jsonValue, String configId) throws Exception{
        if (StringUtil.isBlank(jsonValue)) {
            return null;
        }
        JSONObject jsonObject;
        jsonObject = JSONObject.parseObject(jsonValue, Feature.OrderedField);
        return this.convertXbrlXml(jsonObject, configId);
    }
    /**
     * 转换实例文档xml数据
     * @param jsonObject
     * @param configId
     * @return
     */
    public String convertXbrlXml(JSONObject jsonObject, String configId) throws Exception{

        Xbrl xbrlData = this.createXbrlData(jsonObject, configId);
        if (xbrlData == null) {
            throw new Exception("生成xbrl数据对象为空，无法生成文件");
        }

        if (xbrlOutputer != null) {
            return xbrlOutputer.getXbrlXml(xbrlData);
        }
        return null;
    }

    /**
     * 生成实例文档文件
     * @param savePath
     * @param fileName
     * @param jsonValue
     * @param configId
     * @throws Exception
     */
    public void createXbrlFile(String jsonValue, String configId, String savePath, String fileName) throws Exception{
        if (StringUtil.isBlank(jsonValue)) {
            return;
        }
        JSONObject jsonObject;
        jsonObject = JSONObject.parseObject(jsonValue, Feature.OrderedField);
        this.createXbrlFile(jsonObject, configId, savePath, fileName);
    }
    /**
     * 生成实例文档文件
     * @param savePath
     * @param fileName
     * @param jsonObject
     * @param configId
     * @throws Exception
     */
    public void createXbrlFile(JSONObject jsonObject, String configId, String savePath, String fileName) throws Exception{

        Xbrl xbrlData = this.createXbrlData(jsonObject, configId);
        if (xbrlData == null) {
            throw new Exception("生成xbrl数据对象为空，无法生成文件");
        }

        if (xbrlOutputer != null) {
            xbrlOutputer.exportXbrl(xbrlData, savePath, fileName, false);
        }
    }



    public XbrlOutputer getXbrlOutputer() {
        return xbrlOutputer;
    }

    public void setXbrlOutputer(XbrlOutputer xbrlOutputer) {
        this.xbrlOutputer = xbrlOutputer;
    }
}
