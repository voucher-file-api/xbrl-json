package com.pansoft.xbrl.xbrljson.convert;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pansoft.xbrl.xbrljson.util.StringUtil;
import org.dom4j.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * xml转换为json
 * @author coolmayi
 * @create 2022/9/22
 */
public class DomToJson {

    private static Logger log = Logger.getLogger(DomToJson.class.toString());

    /**
     * 将XML数据转换为json数据
     * @param xmlValue
     * @return
     */
    public JSONObject convertXmlJsonData(String xmlValue) {

        if (StringUtil.isBlank(xmlValue)) {
            return null;
        }
        Document document = null;
        JSONObject resultJSON = new JSONObject();


        try {
            document = DocumentHelper.parseText(xmlValue);

            Element rootNode = document.getRootElement();

            if (rootNode == null) {
                return null;
            }

            JSONObject jsonObject = new JSONObject();

            this.readJsonData(rootNode, jsonObject);
            resultJSON.put(rootNode.getName(), jsonObject);

        } catch (DocumentException e) {
            log.info(e.toString());
        }
        return resultJSON;
    }

    /**
     * dom节点转换为json对象
     * @param element
     * @param jsonObject
     */
    private void readJsonData(Element element, JSONObject jsonObject) {

        if (element == null || jsonObject == null) {
            return ;
        }

        // 处理节点属性
        List<Attribute> attrList = element.attributes();
        if (attrList != null && attrList.size() > 0) {
            String attrValue = null;
            for (Attribute attr : attrList) {
                attrValue = attr.getValue();
                if (StringUtil.isBlank(attrValue)) {
                    continue;
                }
                jsonObject.put("@" + attr.getName(), attrValue);
            }
        }

        // 处理节点下级节点
        List<Element> childList = element.elements();
        String textValue = element.getText();
        if (!StringUtil.isBlank(textValue)) {
            jsonObject.put(element.getName(), textValue);
        }

        if (childList == null || childList.size() == 0) {
            return ;
        }

        String itemName = null;
        List<Element> childNodeList = null;
        for (Element item : childList) {
            itemName = item.getName();
            childNodeList = item.elements();
            attrList = item.attributes();

            if (childNodeList == null || childNodeList.size() == 0) {

                textValue = item.getText();
                if (!StringUtil.isBlank(textValue)) {
                    jsonObject.put(itemName, textValue);
                }
                // 没有下级
                if (attrList == null) {
                    continue;
                }
                String attrValue = null;
                for (Attribute attr : attrList) {
                    attrValue = attr.getValue();
                    if (StringUtil.isBlank(attrValue)) {
                        continue;
                    }
                    jsonObject.put("@" + attr.getName(), attrValue);
                }
            } else {
                // 存在下级

                JSONObject childJson = new JSONObject();
                // 递归调用下级
                readJsonData(item, childJson);

                Object o = jsonObject.get(itemName);

                if (o != null) {

                    JSONArray jsonArray = null;

                    if (o instanceof JSONObject) {
                        // 如果元素已经存在，判定为数组
                        JSONObject jsonObj = (JSONObject)o;
                        jsonObject.remove(itemName);
                        jsonArray = new JSONArray();
                        jsonArray.add(jsonObj);
                        jsonArray.add(childJson);
                        jsonObject.put(itemName, jsonArray);
                    }

                    if (o instanceof JSONArray) {
                        jsonArray = (JSONArray)o;
                        jsonArray.add(childJson);
                    }

                } else {
                    if (childJson != null && !childJson.isEmpty()) {
                        jsonObject.put(itemName, childJson);
                    }
                }
            }
        }
    }
}
