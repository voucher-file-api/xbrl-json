package com.pansoft.xbrl.xbrljson.reader;

import com.alibaba.fastjson.JSONObject;
import org.dom4j.DocumentException;

/**
 * xbrl读取器
 * @author coolmayi
 * @create 2021/9/6
 */
public interface XbrlReader {

    /**
     * 根据路径读取xbrl数据
     * @param xbrlFilePath
     * @return
     * @throws Exception
     */
    public JSONObject readXbrlDataWithPath(String xbrlFilePath) throws DocumentException;

    /**
     * 根据xml内容入去xbrl数据
     * @param xmlValue
     * @return
     * @throws Exception
     */
    public JSONObject readXbrlDataWithXml(String xmlValue) throws Exception;

}
