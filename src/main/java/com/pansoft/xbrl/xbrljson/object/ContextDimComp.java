package com.pansoft.xbrl.xbrljson.object;

import com.pansoft.xbrl.xbrljson.model.ContextDim;

import java.util.Comparator;

/**
 * 维度排序，根据维度编号排序
 * @author coolmayi
 *
 */
public class ContextDimComp implements Comparator<ContextDim>{


	@Override
	public int compare(ContextDim o1, ContextDim o2) {
		// TODO Auto-generated method stub
		
		if (o1 == null || o1.getDimension() == null) {
			return -1;
		}
		
		if (o2 == null || o2.getDimension() == null) {
			return 1;
		}
		
		String dim1 = o1.getDimension() == null ? "" : o1.getDimension();
		String dim2 = o2.getDimension() == null ? "" : o2.getDimension();
		
		return dim1.compareTo(dim2);
	}

}
