package api;

import api.loader.OFDLoader;
import api.tools.PDFTool;
import com.alibaba.fastjson.JSONObject;
import com.pansoft.xbrl.xbrljson.convert.DomToJson;
import com.pansoft.xbrl.xbrljson.convert.JsonToXbrl;
import com.pansoft.xbrl.xbrljson.convert.XbrlToJson;
import com.pansoft.xbrl.xbrljson.util.PropUtil;


public class VoucherFileUtil {

    static{
        PropUtil.initFileMap("VoucherType"); // 加载票据类型
    }
    /**
     * 从OFD中解析出XBRL文件
     * @param ofdFilePath ofd文件路径
     * @return XBRL文件
     */
    public static VoucherFileInfo extractXBRLFromOFD(String ofdFilePath) throws Exception{
        return extractXBRLFromOFD(ofdFilePath,"");
    }

    /**
     * 从OFD中解析出XBRL文件
     * @param ofdFilePath ofd文件路径
     * @param outputFile xbrl文件存储名称
     * @return XBRL文件
     */
    public static VoucherFileInfo extractXBRLFromOFD(String ofdFilePath, String outputFile) throws Exception{
        return new OFDLoader().extractAttach(ofdFilePath,outputFile);
    }

    /**
     * 从PDF中提取XBRL文件，仅支持非税缴款书
     * @param pdfFilePath pdf文件路径
     * @return XBRL文件
     */
    @Deprecated
    public static VoucherFileInfo extractXBRLFromPDF(String pdfFilePath){
        return extractXBRLFromPDF(pdfFilePath, "");
    }

    /**
     * 从PDF中提取XBRL文件，仅支持非税缴款书
     * @param pdfFilePath pdf文件路径
     * @return XBRL文件
     */
    @Deprecated
    public static VoucherFileInfo extractXBRLFromPDF(String pdfFilePath, String outputFile){
        String DEFAULT_ISSUER_XBRL_FILENAME = "voucher_issuer.xbrl";
        return PDFTool.extractXBRLFileFromPDF(pdfFilePath, DEFAULT_ISSUER_XBRL_FILENAME, outputFile);
    }

    /**
     * 从PDF中提取附件
     * @param pdfFilePath pdf文件路径
     * @param outputPath 输出文件目录
     */
    public static void extractAttachFromPDF(String pdfFilePath, String outputPath){
        PDFTool.extractAttachFromPDF(pdfFilePath, outputPath);
    }

    /**
     * 从PDF中提取XML文件，适用于国库集中支付电子凭证
     * @param pdfFilePath pdf文件路径
     */
    public static String extractXMLFromPDF(String pdfFilePath) throws Exception {
        return PDFTool.extractEmbeddedStringFromPDF(pdfFilePath);
    }

    /**
     * xbrl转json
     * @param xbrlXml xbrl文件的内容
     * @param configId 模板编号
     * @return Json数据对象
     */
    public static JSONObject xbrl2Json(String xbrlXml, String configId){
        return new XbrlToJson().convertXbrlJsonData (xbrlXml, configId);
    }

    /**
     * json转xbrl
     * @param jsonValue json数据
     * @param configId 模板编号
     * @return xbrl数据
     */
    public static String json2Xbrl(String jsonValue, String configId) throws Exception {
        return new JsonToXbrl().convertXbrlXml(jsonValue, configId);
    }

    /**
     * xml数据转换为 json数据
     * @param xmlValue xml数据
     * @return jsonObject
     */
    public static JSONObject xml2Json(String xmlValue){
        return new DomToJson().convertXmlJsonData(xmlValue);
    }
}
