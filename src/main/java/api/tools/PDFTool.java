package api.tools;

import api.VoucherFileInfo;
import com.pansoft.xbrl.xbrljson.util.CleanPathUtil;
import org.apache.pdfbox.cos.*;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDEmbeddedFilesNameTreeNode;
import org.apache.pdfbox.pdmodel.common.PDNameTreeNode;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import org.apache.pdfbox.pdmodel.common.filespecification.PDEmbeddedFile;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.apache.commons.codec.binary.Base64;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * PDF提取工具
 */
public class PDFTool {
    private static final Logger log = Logger.getLogger(PDFTool.class.toString());

    /**
     * 从PDF中提取XML文件
     * @param pdfPath pdf 文件路径
     * @return xml 文件内容
     */
    public static String extractEmbeddedStringFromPDF(String pdfPath) throws Exception {
        File file = new File(pdfPath);
        PDDocument document = PDDocument.load(file);
        COSDocument cosDocument = document.getDocument();
        COSDictionary trailer = cosDocument.getTrailer();
        COSObject cosObject = trailer.getCOSObject(COSName.getPDFName("Root"));
        COSBase base = cosObject.getDictionaryObject(COSName.getPDFName("Attachment"));
        String attachmentData = ((COSDictionary) base).getString("AttachmentData");
        byte[] decodedBytes = Base64.decodeBase64(attachmentData.getBytes());
        String decodedString = new String(decodedBytes);

        DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = fac.newDocumentBuilder();

        InputSource is = new InputSource(new StringReader(decodedString));
        Document xmlDocument = builder.parse(is);

        String voucherString = xmlDocument.getDocumentElement().getElementsByTagName("Voucher").item(0).getTextContent();
        byte[] decodedVoucherBytes = Base64.decodeBase64(voucherString.getBytes());
        return new String(decodedVoucherBytes,"GBK");
    }

    /**
     * 从PDF中提取XBRL文件
     * @param pdfPath pdf 文件路径
     * @param xbrlFileName xbrl 文件名
     * @return VoucherFileInfo
     */
    public static VoucherFileInfo extractXBRLFileFromPDF(String pdfPath, String xbrlFileName, String outputFile) {
        PDDocument document = null;
        try {
            document = PDDocument.load(new File(pdfPath));
            PDDocumentNameDictionary namesDictionary = new PDDocumentNameDictionary(document.getDocumentCatalog());
            PDEmbeddedFilesNameTreeNode efTree = namesDictionary.getEmbeddedFiles();
            if (efTree != null) {
                Map<String, PDComplexFileSpecification> names = efTree.getNames();
                if (names != null) {
                    return extractFiles(names, xbrlFileName, outputFile);
                } else {
                    List<PDNameTreeNode<PDComplexFileSpecification>> kids = efTree.getKids();
                    for (PDNameTreeNode<PDComplexFileSpecification> node : kids) {
                        names = node.getNames();
                        return extractFiles(names, xbrlFileName, outputFile);
                    }
                }
            }
        } catch (IOException e) {
            log.info("Exception while trying to read pdf document - "+e);
        } finally {
            try {
                if (document != null) {
                    document.close();
                }
            } catch (IOException e) {
                //e.printStackTrace();
                log.info(e.toString());
            }
        }
        return null;
    }

    /**
     * 从PDF中提取附件文件
     * @param pdfPath pdf 文件路径
     * @param outputPath 输出路径
     */
    public static void extractAttachFromPDF(String pdfPath,  String outputPath) {
        PDDocument document = null;
        try {
            document = PDDocument.load(new File(pdfPath));
            PDDocumentNameDictionary namesDictionary = new PDDocumentNameDictionary(document.getDocumentCatalog());
            PDEmbeddedFilesNameTreeNode efTree = namesDictionary.getEmbeddedFiles();
            if (efTree != null) {
                Map<String, PDComplexFileSpecification> names = efTree.getNames();
                if (names != null) {
                    extractFiles(names, outputPath);
                } else {
                    List<PDNameTreeNode<PDComplexFileSpecification>> kids = efTree.getKids();
                    for (PDNameTreeNode<PDComplexFileSpecification> node : kids) {
                        names = node.getNames();
                        extractFiles(names, outputPath);
                    }
                }
            }
        } catch (IOException e) {
            log.info("Exception while trying to read pdf document - "+e);
        } finally {
            try {
                if (document != null) {
                    document.close();
                }
            } catch (IOException e) {
                log.info(e.toString());
            }
        }
    }

    private static void extractFiles(Map<String, PDComplexFileSpecification> names, String outputPath) throws IOException {
        for (Map.Entry<String, PDComplexFileSpecification> entry : names.entrySet()) {
            PDComplexFileSpecification fileSpec = entry.getValue();
            String filename = fileSpec.getFile();

            PDEmbeddedFile embeddedFile = getEmbeddedFile(fileSpec);

            String outFile;
            if(outputPath == null || "".equals(outputPath)){
                outFile = System.getProperty("java.io.tmpdir") + File.separator + filename;
            }else {
                outFile = outputPath + File.separator + filename;
            }
            //过滤路径特殊字符
            outFile = CleanPathUtil.cleanString(outFile);
            File file = new File(outFile);
            log.info("Writing " + outFile);
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(file);
                fos.write(embeddedFile.toByteArray());
            }catch (IOException e) {
                log.info("Exception while trying to read pdf document - " + e);
            }finally {
                if(null!=fos){
                    fos.close();
                }
            }
        }
    }

    private static VoucherFileInfo extractFiles(Map<String, PDComplexFileSpecification> names, String attachName, String outputFile) throws IOException {
        for (Map.Entry<String, PDComplexFileSpecification> entry : names.entrySet()) {
            PDComplexFileSpecification fileSpec = entry.getValue();
            String filename = fileSpec.getFile();

            if(attachName.equals( filename)){
                PDEmbeddedFile embeddedFile = getEmbeddedFile(fileSpec);
                return extractFile(filename, embeddedFile, outputFile);
            }
        }
        return null;
    }

    private static VoucherFileInfo extractFile(String attachFilename, PDEmbeddedFile embeddedFile, String outputFile) throws IOException {

        VoucherFileInfo voucherFileInfo = new VoucherFileInfo();
        voucherFileInfo.setVoucherType("ntrev_gpm_issuer");
        String outFilePath = outputFile;
        if(outFilePath == null || "".equals(outFilePath)){
            outFilePath = System.getProperty("java.io.tmpdir") + File.separator + attachFilename;
        }
        //过滤路径特殊字符
        outFilePath = CleanPathUtil.cleanString(outFilePath);
        File file = new File(outFilePath);
        log.info("Writing " + outFilePath);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(embeddedFile.toByteArray());
            //fos.close();
        }catch (IOException e) {
            log.info("Exception while trying to read pdf document - " + e);
        }finally {
            if(null!=fos){
                fos.close();
            }
        }

        voucherFileInfo.setXbrlFilePath(outFilePath);
        return voucherFileInfo;//new File(embeddedFilename);
    }

    private static PDEmbeddedFile getEmbeddedFile(PDComplexFileSpecification fileSpec) {
        PDEmbeddedFile embeddedFile = null;
        if (fileSpec != null) {
            embeddedFile = fileSpec.getEmbeddedFileUnicode();
            if (embeddedFile == null) {
                embeddedFile = fileSpec.getEmbeddedFileDos();
            }
            if (embeddedFile == null) {
                embeddedFile = fileSpec.getEmbeddedFileMac();
            }
            if (embeddedFile == null) {
                embeddedFile = fileSpec.getEmbeddedFileUnix();
            }
            if (embeddedFile == null) {
                embeddedFile = fileSpec.getEmbeddedFile();
            }
        }
        return embeddedFile;
    }
}
