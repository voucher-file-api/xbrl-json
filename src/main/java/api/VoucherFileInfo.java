package api;

public class VoucherFileInfo {
    private String voucherType;
    private String xbrlFilePath;

    public VoucherFileInfo(){
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public String getXbrlFilePath() {
        return xbrlFilePath;
    }

    public void setXbrlFilePath(String xbrlFilePath) {
        this.xbrlFilePath = xbrlFilePath;
    }
}
